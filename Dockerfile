FROM hashicorp/packer as packer

FROM amazonlinux

COPY --from=packer /bin/packer /bin/packer

RUN yum -y install zip unzip curl python3 && \
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    aws --version && \
    yum clean all && \
    rm -rf /var/cache/yum